package no.accelerate.java_web_api.mapper;

import no.accelerate.java_web_api.model.CharacterMovie;
import no.accelerate.java_web_api.model.Movie;
import no.accelerate.java_web_api.model.dto.characterDTO.CharacterMovieDTO;
import no.accelerate.java_web_api.model.dto.movieDTO.MovieDTO;
import no.accelerate.java_web_api.model.dto.movieDTO.MoviesGetFromFranchiseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface CharacterMovieMapper {
    @Mapping(target = "movies", qualifiedByName = "moviesToIds")
    CharacterMovieDTO characterMovieToCharacterMovieDTO(CharacterMovie character);
    Collection<CharacterMovieDTO> characterMoviesToCharacterMovieDTOs(Collection<CharacterMovie> character);

    @Named("moviesToIds")
    default Set<Integer> map(Set<Movie> source){
        if(source == null)
            return null;
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }

    @Mapping(target = "movies", qualifiedByName = "moviesToSpecificMovies")
    Collection<MoviesGetFromFranchiseDTO> moviesDTOToMoviesGetFromFranchiseDTO(Collection<Movie> movie);

    @Named("moviesToSpecificMovies")
    default Set<MoviesGetFromFranchiseDTO> mapMovies(Set<MoviesGetFromFranchiseDTO> source){
        if(source == null)
            return null;
        return source.stream().collect(Collectors.toSet());
    }


}
