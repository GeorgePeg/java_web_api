package no.accelerate.java_web_api.mapper;

import no.accelerate.java_web_api.model.Franchise;
import no.accelerate.java_web_api.model.Movie;
import no.accelerate.java_web_api.model.dto.franchiseDTO.FranchiseDTO;
import no.accelerate.java_web_api.model.dto.movieDTO.MoviesGetFromFranchiseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface FranchiseMapper {
    @Mapping(target = "movies", qualifiedByName = "moviesToIds")
    FranchiseDTO franchiseToFranchiseDTO(Franchise franchise);
    Collection<FranchiseDTO> franchisesToFranchiseDTOs(Collection<Franchise> franchise);

    @Named("moviesToIds")
    default Set<Integer> map(Set<Movie> source){
        if(source == null)
            return null;
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }

    @Mapping(target = "movies", qualifiedByName = "moviesToSpecificMovies")
    Collection<MoviesGetFromFranchiseDTO> moviesToMoviesGetFromFranchiseDTO(Collection<Movie> movies);

    @Named("moviesToSpecificMovies")
    default Set<MoviesGetFromFranchiseDTO> mapOther(Set<MoviesGetFromFranchiseDTO> source){
        if(source == null)
            return null;
        return source.stream().collect(Collectors.toSet());
    }
}
