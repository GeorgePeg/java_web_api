package no.accelerate.java_web_api.mapper;

import no.accelerate.java_web_api.model.CharacterMovie;
import no.accelerate.java_web_api.model.Franchise;
import no.accelerate.java_web_api.model.Movie;
import no.accelerate.java_web_api.model.dto.movieDTO.MovieDTO;
import no.accelerate.java_web_api.model.dto.characterDTO.CharactersGetFromMovieDTO;
import no.accelerate.java_web_api.model.dto.franchiseDTO.FranchiseGetFromMovieDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface MovieMapper {
    @Mapping(target = "characterMovies", qualifiedByName = "charactersToIds")
    @Mapping(target = "franchise", source = "franchise.id")
    MovieDTO movieToMovieDTO(Movie movie);
    Collection<MovieDTO> moviesToMovieDTOs(Collection<Movie> movie);

    @Named("charactersToIds")
    default Set<Integer> map(Set<CharacterMovie> source){
        if(source == null)
            return null;
        return source.stream().map(s -> s.getId()).collect(Collectors.toSet());
    }

    @Mapping(target = "characterMovies", qualifiedByName = "charactersToSpecificCharacters")
    @Mapping(target = "franchise", source = "franchise.id")
    Collection<CharactersGetFromMovieDTO> CharacterMovieToCharacterGetFromMovieDTO(Collection<CharacterMovie> characters);

    @Named("charactersToSpecificCharacters")
    default Set<CharactersGetFromMovieDTO> mapOther(Set<CharactersGetFromMovieDTO> source){
        if(source == null)
            return null;
        return source.stream().collect(Collectors.toSet());
    }

    FranchiseGetFromMovieDTO franchiseToFranchiseGetFromMovieDTO(Franchise franchise);

}
