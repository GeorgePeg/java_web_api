package no.accelerate.java_web_api.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "movie")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private int id;
    @Column(name = "movie_title", length = 70, nullable = false)
    private String title;
    @Column(name = "movie_genres", length = 20)
    private String genre;
    @Column(name = "movie_release_year")
    private int releaseYear;
    @Column(name = "movie_director_name", length = 70)
    private String director;
    @Column(name = "movie_picture_url", length = 150)
    private String pictureURL;
    @Column(name = "movie_trailer_url", length = 150)
    private String trailerURL;
    @ManyToMany
    @JoinTable( name = "character_movie",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")} )
    private Set<CharacterMovie> characterMovies;
    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;
}
