package no.accelerate.java_web_api.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "franchise")
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "franchise_id")
    private int id;
    @Column(name = "franchise_name", length = 70, nullable = false)
    private String name;
    @Column(name = "franchise_ description")
    private String description;
    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;
}
