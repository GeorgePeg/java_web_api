package no.accelerate.java_web_api.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "character")
public class CharacterMovie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "character_id")
    private int id;
    @Column(name = "character_full_name", length = 70, nullable = false)
    private String fullName;
    @Column(name = "character_alias", length = 35)
    private String alias;
    @Column(name = "character_gender", length = 20)
    private String gender;
    @Column(name = "character_picture_url", length = 150)
    private String pictureURL;
    @ManyToMany(mappedBy = "characterMovies")
    private Set<Movie> movies;
}
