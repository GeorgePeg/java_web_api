package no.accelerate.java_web_api.model.dto.characterDTO;

import lombok.Getter;
import lombok.Setter;

import java.util.Set;
@Getter
@Setter
public class CharacterMovieDTO {
    private int id;
    private String fullName;
    private String alias;
    private String gender;
    private String pictureURL;
    private Set<Integer> movies;
}
