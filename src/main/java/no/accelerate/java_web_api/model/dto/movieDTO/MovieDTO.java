package no.accelerate.java_web_api.model.dto.movieDTO;

import lombok.Getter;
import lombok.Setter;
import java.util.Set;

@Getter
@Setter
public class MovieDTO {
    private int id;
    private String title;
    private String genre;
    private int releaseYear;
    private String director;
    private String pictureURL;
    private String trailerURL;
    private Set<Integer> characterMovies;
    private Integer franchise;
}
