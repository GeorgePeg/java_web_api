package no.accelerate.java_web_api.model.dto.movieDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoviesGetFromFranchiseDTO {
    private int id;
    private String title;
    private String genre;
    private int releaseYear;
    private String director;
    private String pictureURL;
    private String trailerURL;
}
