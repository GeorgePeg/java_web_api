package no.accelerate.java_web_api.model.dto.characterDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CharactersGetFromMovieDTO {
    private int id;
    private String fullName;
    private String alias;
    private String gender;
    private String pictureURL;

}
