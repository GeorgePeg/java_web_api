package no.accelerate.java_web_api.model.dto.franchiseDTO;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FranchiseGetFromMovieDTO {
    private int id;
    private String name;
    private String description;
}
