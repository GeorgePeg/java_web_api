package no.accelerate.java_web_api.repository;

import no.accelerate.java_web_api.model.CharacterMovie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<CharacterMovie, Integer> {
}
