package no.accelerate.java_web_api.service.character;

import no.accelerate.java_web_api.model.CharacterMovie;
import no.accelerate.java_web_api.model.Movie;
import no.accelerate.java_web_api.service.CrudService;

import java.util.Set;


public interface CharacterService extends CrudService<CharacterMovie, Integer> {
    public String getFullNameById(Integer id);
    public String getAliasById(Integer id);
    public String getGenderById(Integer id);
    public String getPictureURLById(Integer id);
    Set<Movie> getMoviesById(Integer id);
}
