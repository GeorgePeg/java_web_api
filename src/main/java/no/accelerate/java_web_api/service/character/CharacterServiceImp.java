package no.accelerate.java_web_api.service.character;

import no.accelerate.java_web_api.model.Movie;
import no.accelerate.java_web_api.model.CharacterMovie;
import no.accelerate.java_web_api.repository.CharacterRepository;
import no.accelerate.java_web_api.repository.MovieRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CharacterServiceImp implements CharacterService {
    private CharacterRepository characterRepository;
    private MovieRepository movieRepository;

    public CharacterServiceImp(CharacterRepository characterRepository, MovieRepository movieRepository) {
        this.characterRepository = characterRepository;
        this.movieRepository = movieRepository;
    }

    @Override
    public CharacterMovie findById(Integer id) {
        return this.characterRepository.findById(id).orElseThrow();
    }

    @Override
    public Collection<CharacterMovie> findAll() {
        return this.characterRepository.findAll();
    }

    @Override
    public CharacterMovie add(CharacterMovie character) {
        return this.characterRepository.save(character);
    }

    @Override
    public CharacterMovie update(CharacterMovie character) {
        return this.characterRepository.save(character);
    }

    @Override
    public void deleteById(Integer id) {
        for(Movie m : this.movieRepository.findAll())
            if(m.getCharacterMovies().contains(this.characterRepository.findById(id).get())){
                Set<CharacterMovie> remainCharacters = m.getCharacterMovies()
                                                        .stream()
                                                        .filter(c -> c.getId() != id)
                                                        .collect(Collectors.toSet());
                m.setCharacterMovies(remainCharacters);
                this.movieRepository.save(m);
            }
        this.characterRepository.deleteById(id);
    }

    @Override
    public boolean exists(Integer id) {
        return this.characterRepository.existsById(id);
    }

    public String getFullNameById(Integer id){
        return this.characterRepository.findById(id).get().getFullName();
    }

    public String getAliasById(Integer id){
        return this.characterRepository.findById(id).get().getAlias();
    }

    public String getGenderById(Integer id){
        return this.characterRepository.findById(id).get().getGender();
    }

    public String getPictureURLById(Integer id){
        return this.characterRepository.findById(id).get().getPictureURL();
    }

    public Set<Movie> getMoviesById(Integer id){
        Set<Movie> movieTitleCollection = new HashSet<>();
        for(Movie m : this.characterRepository.findById(id).get().getMovies())
            movieTitleCollection.add(m);
        return movieTitleCollection;
    }
}
