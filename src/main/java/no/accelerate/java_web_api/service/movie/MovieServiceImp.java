package no.accelerate.java_web_api.service.movie;

import no.accelerate.java_web_api.model.Franchise;
import no.accelerate.java_web_api.model.Movie;
import no.accelerate.java_web_api.model.CharacterMovie;
import no.accelerate.java_web_api.repository.CharacterRepository;
import no.accelerate.java_web_api.repository.MovieRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
public class MovieServiceImp implements MovieService {
    private MovieRepository movieRepository;
    private CharacterRepository characterRepository;

    public MovieServiceImp(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    @Override
    public Movie findById(Integer id) {
        return this.movieRepository.findById(id).orElseThrow();
    }

    @Override
    public Collection<Movie> findAll() {
        return this.movieRepository.findAll();
    }

    @Override
    public Movie add(Movie movie) {
        return this.movieRepository.save(movie);
    }

    @Override
    public Movie update(Movie movie) {
        return this.movieRepository.save(movie);
    }

    @Override
    public void deleteById(Integer id) {
        this.movieRepository.deleteById(id);
    }

    @Override
    public boolean exists(Integer id) {
        return this.movieRepository.existsById(id);
    }

    public String getTitleById(Integer id){
        return this.movieRepository.findById(id).get().getTitle();
    }

    public String getGenreById(Integer id){
        return this.movieRepository.findById(id).get().getGenre();
    }

    public Integer getReleaseYearById(Integer id){
        return this.movieRepository.findById(id).get().getReleaseYear();
    }

    public String getDirectorById(Integer id){
        return this.movieRepository.findById(id).get().getDirector();
    }

    public String getPictureURLById(Integer id){
        return this.movieRepository.findById(id).get().getPictureURL();
    }

    public String getTrailerURLById(Integer id){
        return this.movieRepository.findById(id).get().getTrailerURL();
    }

    public Set<CharacterMovie> getMovieCharactersById(Integer id){
        Set<CharacterMovie> characters = new HashSet<>();
        for(CharacterMovie c : this.movieRepository.findById(id).get().getCharacterMovies())
            characters.add(c);
        return characters;
    }

    public Franchise getFranchiseById(Integer id){
        return this.movieRepository.findById(id).get().getFranchise();
    }

    public void updateCharacters(Integer id, int[] charactersId) {
        Set<CharacterMovie> characterSet = new HashSet<>();
        for(int i : charactersId)
            characterSet.add(this.characterRepository.findById(i).get());
        Movie movie = this.movieRepository.findById(id).get();
        movie.setCharacterMovies(characterSet);
        this.movieRepository.save(movie);
    }
}
