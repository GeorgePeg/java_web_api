package no.accelerate.java_web_api.service.movie;

import no.accelerate.java_web_api.model.CharacterMovie;
import no.accelerate.java_web_api.model.Franchise;
import no.accelerate.java_web_api.model.Movie;
import no.accelerate.java_web_api.service.CrudService;

import java.util.Set;

public interface MovieService extends CrudService<Movie, Integer> {
    public String getTitleById(Integer id);
    public String getGenreById(Integer id);
    public Integer getReleaseYearById(Integer id);
    public String getDirectorById(Integer id);
    public String getPictureURLById(Integer id);
    public String getTrailerURLById(Integer id);
    public Set<CharacterMovie> getMovieCharactersById(Integer id);
    public Franchise getFranchiseById(Integer id);
    public void updateCharacters(Integer id, int[] charactersId);
}
