package no.accelerate.java_web_api.service.franchise;

import no.accelerate.java_web_api.model.CharacterMovie;
import no.accelerate.java_web_api.model.Franchise;
import no.accelerate.java_web_api.model.Movie;
import no.accelerate.java_web_api.service.CrudService;
import java.util.Set;

public interface FranchiseService extends CrudService<Franchise, Integer> {
    public String getNameById(Integer id);
    public String getDescriptionById(Integer id);
    public Set<Movie> getMoviesById(Integer id);
    public Set<CharacterMovie> getCharactersById(Integer id);
    public void updateMovies(Integer id, int[] moviesId);
}
