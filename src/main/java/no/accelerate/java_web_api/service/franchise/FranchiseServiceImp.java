package no.accelerate.java_web_api.service.franchise;

import no.accelerate.java_web_api.model.CharacterMovie;
import no.accelerate.java_web_api.model.Franchise;
import no.accelerate.java_web_api.model.Movie;
import no.accelerate.java_web_api.repository.FranchiseRepository;
import no.accelerate.java_web_api.repository.MovieRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
public class FranchiseServiceImp implements FranchiseService {
    private FranchiseRepository franchiseRepository;
    private MovieRepository movieRepository;

    public FranchiseServiceImp(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    @Override
    public Franchise findById(Integer id) {
        return this.franchiseRepository.findById(id).get();
    }

    @Override
    public Collection<Franchise> findAll() {
        return this.franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise franchise) {
        return this.franchiseRepository.save(franchise);
    }

    @Override
    public Franchise update(Franchise franchise) {
        return this.franchiseRepository.save(franchise);
    }

    @Override
    public void deleteById(Integer id) {
        for(Movie m : this.movieRepository.findAll())
            if(m.getFranchise().getId() == id){
                m.setFranchise(null);
                this.movieRepository.save(m);
            }
        this.franchiseRepository.deleteById(id);
    }

    @Override
    public boolean exists(Integer id) {
        return this.franchiseRepository.existsById(id);
    }

    public String getNameById(Integer id) {
        return this.franchiseRepository.findById(id).get().getName();
    }

    public String getDescriptionById(Integer id) {
        return this.franchiseRepository.findById(id).get().getDescription();
    }

    public Set<Movie> getMoviesById(Integer id){
        Set<Movie> movies = new HashSet<>();
        for(Movie m : this.franchiseRepository.findById(id).get().getMovies())
            movies.add(m);
        return movies;
    }

    public Set<CharacterMovie> getCharactersById(Integer id){
        Set<CharacterMovie> characters = new HashSet<>();
        for(Movie m : this.franchiseRepository.findById(id).get().getMovies())
            for(CharacterMovie c : m.getCharacterMovies())
                characters.add(c);
        return characters;
    }

    public void updateMovies(Integer id, int[] moviesId){
        for(int i : moviesId) {
            Movie m = this.movieRepository.findById(i).get();
            m.setFranchise(this.franchiseRepository.findById(id).get());
            this.movieRepository.save(m);
        }
    }
}
