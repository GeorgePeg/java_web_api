package no.accelerate.java_web_api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.accelerate.java_web_api.mapper.CharacterMovieMapper;
import no.accelerate.java_web_api.model.CharacterMovie;
import no.accelerate.java_web_api.model.dto.characterDTO.CharacterMovieDTO;
import no.accelerate.java_web_api.service.character.CharacterService;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(path = "moviesApi/v1/characters")
public class CharacterController {
    private final CharacterService characterService;
    private final CharacterMovieMapper characterMovieMapper;

    public CharacterController(CharacterService characterService, CharacterMovieMapper characterMovieMapper) {
        this.characterService = characterService;
        this.characterMovieMapper = characterMovieMapper;
    }

    @GetMapping
    @Operation(summary = "Get all characters")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "All characters successfully got",
                    content = {@Content(mediaType = "application/json",
                                        array = @ArraySchema(schema = @Schema(implementation = CharacterMovieDTO.class)))}
            )
    })
    public ResponseEntity findAll(){
        return ResponseEntity.ok(this.characterMovieMapper.characterMoviesToCharacterMovieDTOs(this.characterService.findAll()));
    }

    @GetMapping("{id}")
    @Operation(summary = "Get a character by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Character successfully got",
                    content = {@Content(mediaType = "application/json",
                                        schema = @Schema(implementation = CharacterMovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Character does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                                        schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity findById(@PathVariable int id){
        return ResponseEntity.ok(this.characterMovieMapper.characterMovieToCharacterMovieDTO(this.characterService.findById(id)));
    }

    @PostMapping
    @Operation(summary = "Add a character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Character successfully added",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                                        schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity add(@RequestBody CharacterMovie character) throws URISyntaxException {
        this.characterService.add(character);
        return ResponseEntity.created(new URI("moviesApi/v1/characters" + character.getId())).build();
    }

    @PutMapping("{id}")
    @Operation(summary = "Update a character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Character successfully updated",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterMovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                                        schema = @Schema(implementation = ProblemDetail.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Character does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                                        schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity update(@RequestBody CharacterMovie character)   {
        this.characterService.update(character);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Delete a character")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Character successfully deleted",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Character does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                                        schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity delete(@PathVariable int id) {
        this.characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("{id}/fullName")
    @Operation(summary = "Get character's full name by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Character's name successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterMovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Character does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getFullNameById(@PathVariable int id){
        return ResponseEntity.ok(this.characterService.getFullNameById(id));
    }

    @GetMapping("{id}/alias")
    @Operation(summary = "Get character's alias by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Character's alias successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterMovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Character does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getAliasById(@PathVariable int id){
        return ResponseEntity.ok(this.characterService.getAliasById(id));
    }

    @GetMapping("{id}/gender")
    @Operation(summary = "Get character's gender by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Character's gender successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterMovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Character does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getGenderById(@PathVariable int id){
        return ResponseEntity.ok(this.characterService.getGenderById(id));
    }

    @GetMapping("{id}/pictureURL")
    @Operation(summary = "Get character's URL picture by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Character's URL picture successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterMovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Character does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getPictureURLById(@PathVariable int id){
        return ResponseEntity.ok(this.characterService.getPictureURLById(id));
    }

    @GetMapping("{id}/movies")
    @Operation(summary = "Get character's movies by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Character's movies successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = CharacterMovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Character does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getMoviesById(@PathVariable int id){
        return ResponseEntity.ok(this.characterMovieMapper.moviesDTOToMoviesGetFromFranchiseDTO(this.characterService.getMoviesById(id)));
    }
}
