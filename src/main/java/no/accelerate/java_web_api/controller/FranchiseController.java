package no.accelerate.java_web_api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.accelerate.java_web_api.mapper.FranchiseMapper;
import no.accelerate.java_web_api.mapper.MovieMapper;
import no.accelerate.java_web_api.model.Franchise;
import no.accelerate.java_web_api.model.dto.franchiseDTO.FranchiseDTO;
import no.accelerate.java_web_api.service.franchise.FranchiseService;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(path = "moviesApi/v1/franchises")
public class FranchiseController {
    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;
    private final MovieMapper movieMapper;

    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper, MovieMapper movieMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieMapper = movieMapper;
    }

    @GetMapping
    @Operation(summary = "Get all franchises")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "All franchises successfully got",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))}
            )
    })
    public ResponseEntity findAll(){
        return ResponseEntity.ok(this.franchiseMapper.franchisesToFranchiseDTOs(this.franchiseService.findAll()));
    }

    @GetMapping("{id}")
    @Operation(summary = "Get a franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Franchise successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity findById(@PathVariable int id){
        return ResponseEntity.ok(this.franchiseMapper.franchiseToFranchiseDTO(this.franchiseService.findById(id)));
    }

    @PostMapping
    @Operation(summary = "Add a franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Franchise successfully added",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity add(@RequestBody Franchise franchise) throws URISyntaxException {
        this.franchiseService.add(franchise);
        return ResponseEntity.created(new URI("moviesApi/v1/franchises" + franchise.getId())).build();
    }

    @PutMapping("{id}")
    @Operation(summary = "Update a franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Franchise successfully updated",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity update(@RequestBody Franchise franchise) {
        this.franchiseService.update(franchise);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Delete a franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Franchise successfully deleted",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity delete(@PathVariable int id) {
        this.franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("{id}/name")
    @Operation(summary = "Get franchise's name by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Franchise's name successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getNameById(@PathVariable int id){
        return ResponseEntity.ok(this.franchiseService.getNameById(id));
    }

    @GetMapping("{id}/description")
    @Operation(summary = "Get franchise's description by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Franchise's description successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getDescriptionById(@PathVariable int id){
        return ResponseEntity.ok(this.franchiseService.getDescriptionById(id));
    }

    @GetMapping("{id}/movies")
    @Operation(summary = "Get franchise's movies IDs by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Franchise's movies IDs successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getMoviesById(@PathVariable int id){
        return ResponseEntity.ok(this.franchiseMapper.moviesToMoviesGetFromFranchiseDTO(this.franchiseService.getMoviesById(id)));
    }

    @GetMapping("{id}/characters")
    @Operation(summary = "Get franchise's characters IDs by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Franchise's characters IDs successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getCharactersById(@PathVariable int id){
        return ResponseEntity.ok(this.movieMapper.CharacterMovieToCharacterGetFromMovieDTO(this.franchiseService.getCharactersById(id)));
    }

    @PutMapping("{id}/movies")
    @Operation(summary = "Update movies to a franchise")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Movies successfully updated",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity updateMovies(@PathVariable Integer id, @RequestBody int[] moviesId) {
        this.franchiseService.updateMovies(id, moviesId);
        return ResponseEntity.noContent().build();
    }

}
