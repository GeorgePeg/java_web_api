package no.accelerate.java_web_api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.accelerate.java_web_api.mapper.MovieMapper;
import no.accelerate.java_web_api.model.Movie;
import no.accelerate.java_web_api.model.dto.movieDTO.MovieDTO;
import no.accelerate.java_web_api.service.movie.MovieService;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping(path = "moviesApi/v1/movies")
public class MovieController {
    private final MovieService movieService;
    private final MovieMapper movieMapper;
    public MovieController(MovieService movieService, MovieMapper movieMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
    }

    @GetMapping
    @Operation(summary = "Get all movies")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "All movies successfully got",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = MovieDTO.class)))}
            )
    })
    public ResponseEntity findAll(){
        return ResponseEntity.ok(this.movieMapper.moviesToMovieDTOs(this.movieService.findAll()));
    }

    @GetMapping("{id}")
    @Operation(summary = "Get a movie by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Movie successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity findById(@PathVariable int id){
        return ResponseEntity.ok(this.movieMapper.movieToMovieDTO(this.movieService.findById(id)));
    }

    @PostMapping
    @Operation(summary = "Add a movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "201",
                    description = "Movie successfully added",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity add(@RequestBody Movie movie) throws URISyntaxException {
        this.movieService.add(movie);
        return ResponseEntity.created(new URI("moviesApi/v1/movies" + movie.getId())).build();
    }

    @PutMapping("{id}")
    @Operation(summary = "Update a movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity update(@RequestBody Movie movie) {
        this.movieService.update(movie);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    @Operation(summary = "Delete a movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Movie successfully deleted",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity delete(@PathVariable int id) {
        this.movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("{id}/title")
    @Operation(summary = "Get movie's title by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Movie's title successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getTitleById(@PathVariable int id){
        return ResponseEntity.ok(this.movieService.getTitleById(id));
    }

    @GetMapping("{id}/genre")
    @Operation(summary = "Get movie's genre by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Movie's genre successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getGenreById(@PathVariable int id){
        return ResponseEntity.ok(this.movieService.getGenreById(id));
    }

    @GetMapping("{id}/releaseYear")
    @Operation(summary = "Get movie's release year by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Movie's release year successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getReleaseYearById(@PathVariable int id){
        return ResponseEntity.ok(this.movieService.getReleaseYearById(id));
    }

    @GetMapping("{id}/director")
    @Operation(summary = "Get movie's director name by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Movie's director name successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getDirectorById(@PathVariable int id){
        return ResponseEntity.ok(this.movieService.getDirectorById(id));
    }

    @GetMapping("{id}/pictureURL")
    @Operation(summary = "Get movie's picture URL name by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Movie's picture URL successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getPictureURLById(@PathVariable int id){
        return ResponseEntity.ok(this.movieService.getPictureURLById(id));
    }

    @GetMapping("{id}/trailerURL")
    @Operation(summary = "Get movie's trailer URL name by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Movie's trailer URL successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getTrailerURLById(@PathVariable int id){
        return ResponseEntity.ok(this.movieService.getTrailerURLById(id));
    }

    @GetMapping("{id}/characters")
    @Operation(summary = "Get movie's characters by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Movie's characters successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getMovieCharactersById(@PathVariable int id){
        return ResponseEntity.ok(this.movieMapper.CharacterMovieToCharacterGetFromMovieDTO(this.movieService.getMovieCharactersById(id)));
    }

    @GetMapping("{id}/franchise")
    @Operation(summary = "Get movie's franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Movie's franchise successfully got",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieDTO.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity getFranchiseById(@PathVariable int id){
        return ResponseEntity.ok(this.movieMapper.franchiseToFranchiseGetFromMovieDTO(this.movieService.getFranchiseById(id)));
    }

    @PutMapping("{id}/characters")
    @Operation(summary = "Update characters to a movie")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "204",
                    description = "Characters successfully updated",
                    content = @Content
            ),
            @ApiResponse(
                    responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            ),
            @ApiResponse(
                    responseCode = "500",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ProblemDetail.class))}
            )
    })
    public ResponseEntity updateCharacters(@PathVariable int id, @RequestBody int[] charactersId) {
        this.movieService.updateCharacters(id, charactersId);
        return ResponseEntity.noContent().build();
    }
}
