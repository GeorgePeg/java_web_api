**Java Web API**
--
The main idea
--
1. Create a database
- Create models
- Create Repositories 
- Create Services to cater 
2. Create a Web API in Spring Web 
- Create controllers
- Swagger/Open API documentation
- Data Transfer Objects with Mapstruct

Set up needed
--
1. IntelliJ with Java 17
- Spring Web
- Spring Data JPA
- PostgreSQL
- Lombok
2. PostgreSQL with PgAdmin
3. Docker – replication of environment

Maintainers
--
 - George Pegias
 - Aris Kardasis
